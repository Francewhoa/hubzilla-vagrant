#!/bin/bash

RSYNCOPTS=$1

# Sync hubzilla core and addons
SRCPATHCORE="/path/to/hubzilla/repo/"
SRCPATHADDONS="/path/to/hubzilla-addons/repo/"
SSHUSER="root"
SSHHOST="hub.localhost"
WEBROOT="/var/www/hub.localhost/"
WWWUSER="www-data"
ADDONREPO="official"
SYNCOPTS="-a --delete"
EXCLUDE="--exclude=.DS_Store --exclude=.git* --exclude=.ht* --exclude=store/ --exclude=extend/ --exclude=addon/"

echo "Create required webroot folders if they do not exist..."
ssh $SSHUSER@$SSHHOST mkdir -p $WEBROOT/store/[data]/smarty3/
ssh $SSHUSER@$SSHHOST chmod -R ug+rw $WEBROOT/store/
ssh $SSHUSER@$SSHHOST mkdir -p $WEBROOT/extend/addon/official/
ssh $SSHUSER@$SSHHOST mkdir -p $WEBROOT/addon/
echo "Sync hubzilla core..."
rsync $SYNCOPTS $EXCLUDE $RSYNCOPTS $SRCPATHCORE "$SSHUSER@$SSHHOST:$WEBROOT/"
echo "Sync hubzilla-addons..."
rsync $SYNCOPTS $EXCLUDE $RSYNCOPTS $SRCPATHADDONS "$SSHUSER@$SSHHOST:$WEBROOT/extend/addon/$ADDONREPO/"
echo "Update addon links..."
ssh $SSHUSER@$SSHHOST "cd $WEBROOT; util/update_addon_repo $ADDONREPO; "
ssh $SSHUSER@$SSHHOST "chown -R $WWWUSER:$WWWUSER $WEBROOT;"

exit 0
