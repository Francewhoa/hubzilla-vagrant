# Hubzilla Development Environment using Vagrant

## Installation and hub administrator registration

1. Clone this repo to a new [Vagrant](https://www.vagrantup.com/) project folder
	```
	git clone https://repository.url/hubzilla-vagrant.git ~/hubzilla-vagrant
	```
1. Add the local domain name for the hub to your host machine's `/etc/hosts` file.

	```
	echo "192.168.33.10 hub.localhost" >> /etc/hosts
	```
1. Launch Vagrant to build the virtual machine (VM). This will take several minutes.
	```
	cd ~/hubzilla-vagrant
	vagrant up
	```
1. Open `http://hub.localhost/register` in your host machine's web browser and register with the admin account credentials:
	```
	admin@example.com
	password
	```
1. Upon registration, you will immediately be directed to http://hub.localhost/new_channel where you can create your administrator account's first channel.

Congratulations, you should now have a fully functioning Hubzilla server running locally in a Virtualbox-hosted VM managed by Vagrant.

## Development workflow

There are many ways you can use this virtual machine to develop your code. The following is but _one_ possibility, which you might find useful. It is completely optional.

### Enable SSH access

To make your development workflow more efficient, you may wish to use an SSH connection to rapidly synchronize the files you are developing in your local (host machine) development environment.

1. On your host machine, copy your public key.
	```
	user@host:~$ cat ~/.ssh/id_rsa.public
		ssh-rsa eLX1UQbJHUCHf2V3K7YlMP0YmIT+50rlEsWre1eobApKb0Ac/WbvssX/Gh/ user@host
	```
1. Use `vagrant ssh` to log in to the virtual machine.
	```
	user@host:~$ cd ~/hubzilla-vagrant
	user@host:hubzilla-vagrant$ vagrant ssh
	```
1. Switch to root and add your host user public key.
	```
	vagrant@jessie:~$ sudo -i
	root@jessie:~# ssh-keygen
			Generating public/private rsa key pair.
			Enter file in which to save the key (/root/.ssh/id_rsa):
			Created directory '/root/.ssh'.
			Enter passphrase (empty for no passphrase):
			Enter same passphrase again:
			Your identification has been saved in /root/.ssh/id_rsa.
			Your public key has been saved in /root/.ssh/id_rsa.pub.
			The key fingerprint is:
			b4:e3:00:40:4f:c5:af:27:05:09:d4:70:29:ac:f8:51 root@jessie
			The key's randomart image is:
			+---[RSA 2048]----+
			| .oo=*oo         |
			|   +E.*          |
			| . oo. o.        |
			|. o  . .o.       |
			| . .  .oS        |
			|  .   oo..       |
			|       o.        |
			|                 |
			|                 |
			+-----------------+
	root@jessie:~# echo "ssh-rsa eLX1UQbJHUCHf2V3K7YlMP0YmIT+50rlEsWre1eobApKb0Ac/WbvssX/Gh/ user@host" >> ~/.ssh/authorized_keys
	```
1. From your host machine, you may now SSH into the virtual machine using
	```
	ssh root@hub.localhost
	```

### Use a script to sync development repo

You can use the included `hubdevsync.sh` script to efficiently synchronize your local development repo with the hub run by the Vagrant virtual machine.

1. Clone the Hubzilla core and addons repositories to a local folder on the host machine which you edit using your favorite development tools.
	```
	git clone https://framagit.org/hubzilla/core.git /path/to/hubzilla/
	git clone https://framagit.org/hubzilla/addons.git /path/to/hubzilla-addons/
	```
1. Modify the `hubdevsync.sh` script variables to match your system. You should only need to modify the two lines below if you did not modify `bootstrap.sh` prior to running `vagrant up`.
	```
	SRCPATHCORE="/path/to/hubzilla/repo/"
	SRCPATHADDONS="/path/to/hubzilla-addons/repo/"
	```
1. Sync changes to the hub by executing the script. Changes should be evident by reloading your browser page.
	```
	user@host:~$ bash ~/hubzilla-vagrant/hubdevsync.sh
	```
