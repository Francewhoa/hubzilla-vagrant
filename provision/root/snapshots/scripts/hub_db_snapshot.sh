#!/bin/bash

if ! [ -f "$1" ]; then
    echo "$1 is not a valid file. Aborting..."
    exit 1
fi
source "$1"
MESSAGE="db snapshot: $2"

if [ "$DBPWD" == "" -o "$SNAPSHOTROOT" == "" -o "$DBNAME" == "" -o "$DBUSER" == "" -o "$HUBROOT" == "" ]; then
    echo "Required variable is not set. Aborting..."
    exit 1
fi

if [ ! -d "$SNAPSHOTROOT"/db/ ]; then
    mkdir -p "$SNAPSHOTROOT"/db/
fi

if  [ ! -d "$SNAPSHOTROOT"/db/ ]; then
    echo "Error creating snapshot directory. Aborting..."
    exit 1
fi

echo "Export database..."
mysqldump -u "$DBUSER" -p"$DBPWD" "$DBNAME" > "$SNAPSHOTROOT"/db/"$DBNAME".sql

cd "$SNAPSHOTROOT"

if [ ! -d ".git" ]; then
    git init
fi
if [ ! -d ".git" ]; then
    echo "Cannot initialize git repo. Aborting..."
    exit 1
fi

git add -A
echo "Commit hub snapshot..."
git commit -a -m "$MESSAGE"

exit 0
