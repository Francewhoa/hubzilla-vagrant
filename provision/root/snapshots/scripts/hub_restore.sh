#!/bin/bash
# Restore hub to a previous state. Input hub config and commit hash

if ! [ -f "$1" ]; then
        echo "$1 is not a valid file. Aborting..."
        exit 1
fi
source "$1"
COMMIT=$2

if [ "$DBPWD" == "" -o "$SNAPSHOTROOT" == "" -o "$DBNAME" == "" -o "$DBUSER" == "" -o "$HUBROOT" == "" ]; then
        echo "Required variable is not set. Aborting..."
        exit 1
fi
RESTOREDIR="$(mktemp -d)/"

if [ ! -d "$RESTOREDIR" ]; then
    echo "Cannot create restore directory. Aborting..."
    exit 1
fi
echo "Cloning the snapshot repo..."
git clone "$SNAPSHOTROOT" "$RESTOREDIR"
cd "$RESTOREDIR"
echo "Checkout requested snapshot..."
git checkout "$COMMIT"
echo "Restore hub root files..."
rsync -a --delete --exclude=.git* "$RESTOREDIR"/www/ "$HUBROOT"/
echo "Restore hub database..."
mysql -u "$DBUSER" -p"$DBPWD" "$DBNAME" < "$RESTOREDIR"/db/"$DBNAME".sql

chown -R www-data:www-data "$HUBROOT"/{store,extend,addon,.htlog,.htconfig.php}

echo "Restored hub to snapshot $COMMIT"
echo "Removing temporary files..."

rm -rf "$RESTOREDIR"

exit 0
