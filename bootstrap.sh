#!/usr/bin/env bash

# Custom configuration
DB_NAME="hubzilla"
DB_USER="hubzilla"
DB_PASS="password"
MYSQL_ROOT_PASS="password"
HUBDIR="hub.localhost"
WEBROOT="/var/www/$HUBDIR/core"
ADDONREPO="official"
WWWUSER="www-data"

# Provisioning actions

# Add dotdeb repository to sources.list for PHP7.0
echo "deb http://packages.dotdeb.org jessie all" | sudo tee -a /etc/apt/sources.list.d/dotdeb.list
echo "deb-src http://packages.dotdeb.org jessie all" | sudo tee -a /etc/apt/sources.list.d/dotdeb.list
# Dotdeb Key for PHP7.0
wget -qO - http://www.dotdeb.org/dotdeb.gpg | sudo apt-key add -
sudo apt-get -y update

echo "Installing Apache.."
sudo apt-get install -y apache2
echo "Installing MariaDB.."
export DEBIAN_FRONTEND=noninteractive
sudo debconf-set-selections <<< "mariadb-server-10.0 mysql-server/root_password password $MYSQL_ROOT_PASS"
sudo debconf-set-selections <<< "mariadb-server-10.0 mysql-server/root_password_again password $MYSQL_ROOT_PASS"
sudo apt-get install -y mariadb-server
echo "Installing git.."
sudo apt-get install -y git
echo "Installing php7.."
sudo apt-get install -y php7.0-zip php7.0-cli php7.0-curl php7.0-gd php7.0-mysql php7.0-mbstring php7.0-xml libapache2-mod-php7.0 php7.0-mcrypt
echo "Installing sendmail.."
sudo apt-get install -y mailutils sendmail-bin

# Enable all the Apache mods
sudo a2enmod rewrite ssl
sudo phpenmod mcrypt zip

# Create the Hubzilla MySQL database
MYSQL=`which mysql`
Q1="CREATE DATABASE IF NOT EXISTS $DB_NAME;"
Q2="GRANT USAGE ON *.* TO $DB_USER@localhost IDENTIFIED BY \"$DB_PASS\";"
Q3="GRANT ALL PRIVILEGES ON $DB_NAME.* TO $DB_USER@localhost;"
Q4="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}${Q3}${Q4}"
$MYSQL -uroot -p$MYSQL_ROOT_PASS -e "$SQL"

# Create the hub webroot and clone the core and addon repositories
sudo git clone https://framagit.org/hubzilla/core.git $WEBROOT
sudo mkdir -p $WEBROOT/store/[data]/smarty3/
sudo chmod -R ug+rw $WEBROOT/store/
cd $WEBROOT
sudo git checkout dev
sudo util/add_addon_repo https://framagit.org/hubzilla/addons.git $ADDONREPO
cd $WEBROOT/extend/addon/$ADDONREPO
sudo git checkout dev
cd $WEBROOT
sudo util/update_addon_repo official
sudo chown -R $WWWUSER:$WWWUSER $WEBROOT;

# Create the Apache config files and restart webserver
sudo rsync -cr /vagrant/provision/etc/apache2/sites-available/ /etc/apache2/sites-available/
sudo sed -i "s/ServerName V_DOMAIN_NAME/ServerName $HUBDIR/g" /etc/apache2/sites-available/*.conf
sudo sed -i "s/V_DOMAIN_NAME/$HUBDIR\/core/g" /etc/apache2/sites-available/*.conf
sudo a2ensite hubzilla
sudo service apache2 restart

# Initialize the database and copy in a config file to bypass the hub setup process.
# Register with email address "admin@example.com" and you can proceed immediately to
# new channel creation because the configuration file disables email verification.
$MYSQL -uroot -p$MYSQL_ROOT_PASS $DB_NAME < $WEBROOT/install/schema_mysql.sql
sudo cp /vagrant/provision/var/www/$HUBDIR/.htconfig.php $WEBROOT/.htconfig.php

# Add a cron job for the Hubzilla poller
CRONLINE="*/10 * * * * cd $WEBROOT; /usr/bin/php Zotlabs/Daemon/Master.php Cron"
(sudo crontab -u $WWWUSER -l; echo "$CRONLINE" ) | sudo crontab -u $WWWUSER -

# Copy the hub snapshot scripts and configuration file
sudo mkdir -p /root/snapshots/{conf,scripts}
sudo rsync -r /vagrant/provision/root/snapshots/conf/ /root/snapshots/conf/
sudo rsync -r /vagrant/provision/root/snapshots/scripts/ /root/snapshots/scripts/

# Add hub domain to local /etc/hosts file
sudo sed -i "s/127.0.0.1\tlocalhost/127.0.0.1\tlocalhost $HUBDIR/" /etc/hosts

exit 0
